extern crate shortct;
extern crate tempdir;

#[cfg(test)]
mod tests {
    use shortct;
    use shortct::Cache;
    use std::env;
    use std::ffi::OsStr;
    use std::fs;
    use std::process::{Command};
    use super::tempdir;

    const BINARY : &'static str ="target/debug/shortct";
    const WRITER : &'static str ="target/debug/writer";

    fn locate(binary : &'static str) -> String {
        env::current_dir().unwrap().as_path().join(binary).to_string_lossy().into_owned()
    }

    fn run(temp_dir : &tempdir::TempDir) -> (String, String, i32) {
        let writer = locate(WRITER);
        let bin = locate(BINARY);
        run_cmd(temp_dir, &bin, &[&writer])
    }
    fn run_stable(temp_dir : &tempdir::TempDir) -> (String, String, i32) {
        let writer = locate(WRITER);
        let bin = locate(BINARY);
        run_cmd(temp_dir, &bin[..], &[&writer[..], "--stable"])
    }
    fn run_cmd<S: AsRef<OsStr>>(temp_dir : &tempdir::TempDir, bin : S, args : &[S] ) -> (String, String, i32) {
        let temp = temp_dir.path().to_str().unwrap();
        let output = Command::new(bin)
            .args(args)
            .current_dir(temp)
            .env("TMPDIR", temp)
            .output()
            .unwrap();
        (String::from_utf8_lossy(&output.stdout).into_owned(),
            String::from_utf8_lossy(&output.stderr).into_owned(),
            output.status.code().unwrap_or(-1))
    }

    fn tmp() -> tempdir::TempDir {
        tempdir::TempDir::new("shortct").unwrap()
    }

    #[test]
    fn run_first_time() {
        let (t1, t2) = (tmp(), tmp());
        let (o1, o2) = (run(&t1), run(&t2));
        assert!(o1 != o2, "{:?} {:?}", o1, o2);
    }

    #[test]
    fn rerun() {
        let temp_dir = tmp();
        assert_eq!(run(&temp_dir), run(&temp_dir));
    }

    #[test]
    fn captures_all_sanity() {
        let (t1, t2) = (tmp(), tmp());
        assert_eq!(run_stable(&t1), run_stable(&t2));
    }

    #[test]
    fn captures_all() {
        let temp_dir = tmp();
        let writer = locate(WRITER);
        assert_eq!(
            run_cmd(&temp_dir, &writer[..], &["--stable"]),
            run_stable(&temp_dir));
    }

    #[test]
    fn cannot_run() {
        let bin = locate(BINARY);
        let temp_dir = tmp();
        let (o,e,c) = run_cmd(&temp_dir, &bin[..], &["oops"]);
        assert_eq!(0, o.len());
        assert!(e.contains("oops"), "e:{}", e);
        assert_eq!(127, c);
    }

    #[test]
    fn cannot_runs_shell() {
        let bin = locate(BINARY);
        let temp_dir = tmp();
        let (o,e,c) = run_cmd(&temp_dir, &bin[..], &["echo 1"]);
        assert_eq!(0, e.len());
        assert_eq!("1\n", o);
        assert_eq!(0, c);
    }

    #[test]
    fn restore_exit_code() {
        let bin = locate(BINARY);
        let temp_dir = tmp();
        let exit_code = 22;
        let r1 = run_cmd(&temp_dir, &bin[..], &[&format!("exit {}", exit_code)]);
        let r2 = run_cmd(&temp_dir, &bin[..], &[&format!("exit {}", exit_code)]);
        assert_eq!(exit_code, r1.2);
        assert_eq!(r1, r2);
    }

    #[test]
    fn implement_forget() {
        let temp_dir = tmp();
        let writer = locate(WRITER);
        let bin = locate(BINARY);
        let first = run_cmd(&temp_dir, &bin, &[&writer]);
        run_cmd(&temp_dir, &bin, &[&writer, &shortct::CMD_FORGET.to_owned()]);
        let second = run_cmd(&temp_dir, &bin, &[&writer]);
        assert!(first != second); 
    }

    #[test]
    fn single_forget() {
        let bin = locate(BINARY);
        let temp_dir = tmp();
        let (o,e,c) = run_cmd(&temp_dir, &bin[..], &[&shortct::CMD_FORGET.to_owned()]);
        assert_eq!(0, o.len());
        assert!(e.contains(shortct::CMD_FORGET), "e:{}", e);
        assert_eq!(127, c);
    }

    #[test]
    fn cannot_write(){
        let temp_dir = tmp();
        let cached = Cache::new(vec!(locate(WRITER)), temp_dir.path()).cache_paths().0;
        fs::create_dir_all(cached).unwrap();
        let (o,e,c) = run(&temp_dir);
        assert_eq!(0, o.len());
        assert!(e.contains("Could not read cached"));
        assert_eq!(101,c);
    }

    #[test]
    fn write_consistent_file() {
        let bin = locate(BINARY);
        let writer = locate(WRITER);
        let temp_dir = tmp();
        let cmd = format!("{}; killall shortct", writer);
        let t1 = run_cmd(&temp_dir, &bin[..], &[&cmd[..]]);
        let t2 = run_cmd(&temp_dir, &bin[..], &[&cmd[..]]);
        assert!( t1 != t2);
    }
}
