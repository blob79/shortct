extern crate rand;
extern crate clap;
use rand::{Rng,XorShiftRng,SeedableRng};
use std::io;
use std::io::prelude::*;
use clap::{App, Arg};
use std::error::Error;

const LINES : u32 = 100;

fn newln(r: &mut XorShiftRng, t:i32, max:i32) -> &'static str {
    let i = r.gen_range(0, max);
    let b = if i == t { true } else { false };
    let n = if b { "\n" } else { "" };
    n
}

fn rng(stable : bool) -> XorShiftRng {
    let seed = [0,1,2,3];
    let mut r : XorShiftRng = rand::weak_rng(); //SeedableRng::from_seed(seed);
    if stable {
        r.reseed(seed); //that's good enought for now
    }
    r
}

fn write_out(r: &mut XorShiftRng) -> Result<(), Box<Error>>{
    try!(writeln!(&mut io::stdout(), "{}{}", r.gen::<u64>(), r.gen::<u64>())); //unique_output
    for i in (0..LINES) {
        let nl = newln(r, 0, 10);
        try!{
            if i % 2 == 0 {
                write!(&mut io::stderr(), "{:05} {}", i, nl)
            } else {
                write!(&mut io::stdout(), "{:05} {}", i, nl)
            }
        }
    }
    Ok(())
}
fn main() {
   let matches = App::new("writer")
        .arg(Arg::with_name("stable")
            .long("stable")
            .help("Have stable output")
            .takes_value(false))
        .get_matches();
    let mut r = rng(matches.occurrences_of("stable") > 0);
    write_out(&mut r).unwrap();
}
