extern crate shortct;
use shortct::*;
use std::process::exit;

fn main() {
    let cached = Cache::from_args();
    if forget() {
        cached.forget().expect("Cannot remove cached file");
    }
    cached.prepare().expect("Could not create cache folder");
    let code = if cached.is_cached() {
        read_cached(cached).expect("Could not read cached")
    } else {
        run(cached).expect("Could not run")
    };
    exit(code);
}
