extern crate rustc_serialize;
extern crate bit_vec;
extern crate byteorder;

use rustc_serialize::base64::{ToBase64,STANDARD};
use std::cmp::min;
use std::env;
use std::error::Error;
use std::fs;
use std::io;
use std::io::prelude::*;
use std::path::{Path,PathBuf};
use std::process::{Command, Stdio, Child};
use std::sync::mpsc;
use std::thread;
use bit_vec::BitVec;
use byteorder::{LittleEndian, ByteOrder, WriteBytesExt};

type Content = Vec<u8>;

pub static BUFFER : usize = 100;
pub static CMD_FORGET : &'static str = "forget";

fn build_cmd() -> Result<Command, io::Error> {
    let shell = env::var("SHELL").expect("Could not find shell");
    if env::args().skip(1).next().is_none() {
        return Err(io::Error::new(io::ErrorKind::Other, "Cmd missing"));
    }
    let mut c : Command = Command::new(shell);
    c.arg("-c");
    c.arg(actual_args().join(" "));
    Ok(c)
}

fn read_buf( read : &mut Read) -> Result<Content, io::Error> {
    let mut buffer : Vec<u8> = vec!(0 ; BUFFER);
    let mut size = 0;
    loop {
        let r = read.read(&mut buffer[size..]).unwrap();
        size += r;
        if r == 0 { break ; }
    }
    buffer.truncate(size);
    Ok(buffer)
}

#[derive (Debug)]
enum Pipe {
    StdOut(Content),
    StdErr(Content),
}

pub struct Cache {
    cmd : Vec<String>,
    temp : PathBuf,
}

impl Cache {
    pub fn new(cmd : Vec<String>, t : &Path) -> Cache {
        Cache { temp: t.to_path_buf(), cmd: cmd, }
    }

    pub fn from_args() -> Cache {
        Cache::new(actual_args(), env::temp_dir().as_path())
    }

    pub fn forget(&self) -> Result<(), io::Error> {
        let (c,o,e) = self.cache_paths();
        fs::remove_file(c).and_then(|_| {fs::remove_file(o)}).and_then(|_| {fs::remove_file(e)})
    }

    pub fn cache_paths(&self) -> (PathBuf, PathBuf, PathBuf) {
        (self.temp_f("-e.c"), self.temp_f("-e.o"), self.temp_f("-e.e"))
    }

    pub fn prepare(&self) -> Option<()> {
        self.cache_path().as_path().parent().and_then( |p| { fs::create_dir_all(p).ok() })

    }

    pub fn is_cached(&self) -> bool {
        ! fs::metadata(self.cache_paths().0.as_path()).is_err()
    }

    fn temp_paths(&self) -> (PathBuf, PathBuf, PathBuf) {
        (self.temp_f("-c"), self.temp_f("-o"), self.temp_f("-e"))
    }

    fn temp_f(&self, suffix : &'static str) -> PathBuf {
         let filename = format!("{}{}", self.cache_path().to_str().unwrap().to_owned(), suffix);
         Path::new(&filename).to_path_buf()
    }

    fn cache_path(&self) -> PathBuf {
        let mut p = self.temp.clone();
        p.push("shortct");
        let ws : Vec<_> = self.cmd.join(" ").into_bytes().to_base64(STANDARD).chars().collect();
        for w in ws.chunks(10) {
            let v : String = w.to_owned().into_iter().collect();
            p.push(format!("f-{}", v)); //- make folders that cannot be base64 encoded values
        }
        p
    }

    fn cache_files(&self) -> Result<(fs::File, fs::File, fs::File), io::Error> {
        let (cached_control, cached_stdout, cached_stderr) = self.cache_paths();
        let f = try!(fs::File::open(cached_control));
        let o = try!(fs::File::open(cached_stdout));
        let e = try!(fs::File::open(cached_stderr));
        Ok((f,o,e))
    }

    fn temp_files(&self) -> Result<(fs::File, fs::File, fs::File), io::Error> {
        let (cached_control, cached_stdout, cached_stderr) = self.temp_paths();
        let f = try!(fs::File::create(cached_control));
        let o = try!(fs::File::create(cached_stdout));
        let e = try!(fs::File::create(cached_stderr));
        Ok((f,o,e))
    }

    fn finish(&self) -> Result<(), io::Error> {
        let (tc, ts, te) = self.temp_paths();
        let (cc, cs, ce) = self.cache_paths();
        try!(fs::rename(tc, cc));
        try!(fs::rename(ts, cs));
        fs::rename(te, ce)
    }
}

fn read_all<F>(read : &mut Read, f : F, c : mpsc::Sender<Pipe>) -> Result<(), Box<Error>>
    where F : Fn(Content) -> Pipe {
   loop {
        let s = try!(read_buf(read));
        if s.len() == 0 {
            return Ok(())
        }
        try!(c.send(f(s)))
    }
}

fn output() -> Result<Child, io::Error> {
    let mut cmd = try!(build_cmd());
    cmd.stdout(Stdio::piped()).stderr(Stdio::piped()).spawn()
}

pub fn run(cached : Cache) -> Result<i32, io::Error>{
    let mut process = try!(output());
    let (txo, rx) = mpsc::channel();
    let txe = txo.clone();

    let (so, se) = (process.stdout.take(), process.stderr.take());

    thread::spawn(move || {
        so.and_then( |mut o| {
            read_all(&mut o, Pipe::StdOut, txo).ok()
        }).expect("Could not read stdout")
    });
    thread::spawn(move || {
        se.and_then( |mut e| {
            read_all(&mut e, Pipe::StdErr, txe).ok()
        }).expect("Could not read stderr")
    });


    let (mut control_file, mut stdout_file, mut stderr_file) = try!(cached.temp_files());
    let mut bv = BitVec::new();
    loop {
        match rx.recv() {
            Ok(v) => {
                match v {
                    Pipe::StdOut(x) => {
                        try!(stdout_file.write(&x));
                        try!(io::stdout().write(&x));
                        bv.push(true);
                    },
                    Pipe::StdErr(x) => {
                        try!(stderr_file.write(&x));
                        try!(io::stderr().write(&x));
                        bv.push(false);
                    },
                };
            }
            _ => {
                let c = try!(process.wait());
                let code = c.code().unwrap_or(-1); // -1 for signals
                let mut header = vec![];
                header.write_i32::<LittleEndian>(code).unwrap();
                header.write_u32::<LittleEndian>(bv.len() as u32).unwrap();
                try!(control_file.write(&header[..]));
                try!(control_file.write(&bv.to_bytes()[..]));

                try!(cached.finish());
                return Ok(code);
            }
        }
    }
}

pub fn read_cached(ca : Cache) -> Result<i32, Box<Error>>{
    let (mut f, mut o, mut e) = try!(ca.cache_files());

    let mut b = vec![];

    try!(f.read_to_end(&mut b));

    let code = LittleEndian::read_i32(&b[0..4]);
    let size = LittleEndian::read_u32(&b[4..8]);
    let mut bv = BitVec::from_bytes(&b[8..]);
    bv.truncate(size as usize);

    let (mut bo, mut be) = (vec![], vec![]);
    try!(o.read_to_end(&mut bo));
    try!(e.read_to_end(&mut be));
    let (mut ie, mut io) = (0,0);

    for x in bv.into_iter() {
        if x {
            let next = min(io + BUFFER, bo.len());
            try!(io::stdout().write(&bo[io..next]));
            io = next;
        } else {
            let next = min(be.len(), ie + BUFFER);
            try!(io::stderr().write(&be[ie..next]));
            ie = next;
        }
    }
    Ok(code)
}

pub fn forget() -> bool {
    if env::args().len() < 3 { return false; }
    match env::args().last() {
        Some(ref l) if l == CMD_FORGET => true,
        _ => false,
    }
}

fn actual_args() -> Vec<String> {
    let f = if forget() { 1 }  else { 0 };
    let vs : Vec<_> = env::args().take(env::args().len() - f).skip(1).collect();
    vs
}

