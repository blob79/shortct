# Shortct

Shortct captures stdout, stderr and exit codes. On successive runs it replays the captured values.

The only change you have to make is to preffix your command with `shortct`. It will do the caching automatically.

Here's a sample session. We run find on a home folder and work with the cached output.
```bash
$ shortct find ~ > /dev/null # the output and exit code are now cached

$ shortct find ~ | grep -o .bashrc # you can work with the cached output
.bashrc

$ shortct find ~ | wc -c # use the output with different command
49874321
```

There are two use cases for shortct you either run a command that is slow or you need stable output that doesn't change with every run.

## Features

### Stable output

The output of shortct is stable. After the first run the output will be replayed. Caching takes all parameter into account. `shortct ls` is different from `shortct ls -l`.
```bash
$ shortct date
Mon Dec 14 19:35:11 SAST 2015
$ shortct date
Mon Dec 14 19:35:11 SAST 2015
```

### Forget cached values
The output is cached unless you clear the cache.
```bash
$ shortct date
Mon Dec 14 19:35:11 SAST 2015
$ shortct date forget
Mon Dec 14 19:35:18 SAST 2015
$ shortct date
Mon Dec 14 19:35:18 SAST 2015
```

### Runs your shell

Shortct runs with your shell.
```bash
$ shortct "echo 1 && echo 2"
1
2
```
### Exit code

Shortct honours the exit code of the command.

```bash
$ shortct false || echo $?
1

$ shortct true && echo $?
0
```

### Stdout and stderr

Shortct will restore your stdout and stderr output.

Shortct tries to honour the order of stdout and stderr output. Keep in mind that the output is subject to buffering - the order is best effort.

```bash
shortct ls not_there >/dev/null #output on stderr
ls: cannot access not_there: No such file or directory
```

### Use stdin

To use stdin use your shell's pipe support. Shortct doesn't read from stdin as ignoring the input would create invalid cache values (invalid: `echo a | shortct grep a`).

```bash
$ shortct 'echo ab | grep -o a'
a
```

